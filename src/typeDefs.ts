const typeDefs = `#graphql
	type Task {
		id: String!
		title: String!
		content: String!
		done: Boolean!
		asignee: Person
	}

	type Person {
		id: String!
		name: String!
		tasks: [Task!]
	}

	type Query {
		getAllPeople: [Person!]
		getAllTasks: [Task!]
	}

	input TaskModificationInput {
		title: String
		content: String
	}

	type Mutation {
		addPerson(name: String!): String!
		addTask(title: String!, content: String!): String!
		assignTask(taskID: String!, personID: String!): String!
		completeTask(taskID: String!): String!
		updateTask(taskID: String!, modification: TaskModificationInput!): Task!
	}
`;

export default typeDefs;
