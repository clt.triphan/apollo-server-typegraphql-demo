export type Task = {
	id: string;
	title: string;
	content: string;
	done: boolean;
	asignee: Person | null;
};

export type Person = {
	id: string;
	name: string;
};

export let people: Person[] = [
	{
		id: "P001",
		name: "Tri Phan",
	},
	{
		id: "P002",
		name: "Nhi Mai",
	},
	{
		id: "P003",
		name: "Ngoc Tran",
	},
	{
		id: "P003",
		name: "Phat Nguyen",
	},
];

export let tasks: Task[] = [
	{
		id: "T001",
		title: "Task number one",
		content: "Please check",
		done: true,
		asignee: people[0],
	},
	{
		id: "T002",
		title: "Internal support",
		content: "Please investigate",
		done: false,
		asignee: people[0],
	},
	{
		id: "T003",
		title: "Implement delete operation",
		content: "Please check",
		done: false,
		asignee: null,
	},
	{
		id: "T004",
		title: "Task ABC",
		content: "No description",
		done: true,
		asignee: people[1],
	},
];
