import { nanoid } from "nanoid";
import { Person, Task, people, tasks } from "./storage";

type AddPersonInput = {
	name: string;
};

type AddTaskInput = {
	title: string;
	content: string;
};

type AssignTaskInput = {
	taskID: string;
	personID: string;
};

type CompleteTaskInput = {
	taskID: string;
};

type TaskModificationInput = {
	title?: string;
	content?: string;
};

type UpdateTaskInput = {
	taskID: string;
	modification: TaskModificationInput;
};

const findTaskByID = (taskID: string): Task => {
	const task = tasks.find((t) => t.id === taskID);

	if (task === undefined) {
		throw new Error("Task not found");
	}

	return task;
};

const resolvers = {
	Query: {
		getAllPeople: () => people,
		getAllTasks: () => tasks,
	},
	Mutation: {
		addPerson: (_: never, { name }: AddPersonInput): string => {
			const id = nanoid(4);
			people.push({ id, name });
			return id;
		},
		addTask: (_: never, { title, content }: AddTaskInput): string => {
			const id = nanoid(4);
			tasks.push({ id, title, content, done: false, asignee: null });
			return id;
		},
		assignTask: (_: never, { taskID, personID }: AssignTaskInput): string => {
			const task = findTaskByID(taskID);

			const person = people.find((p) => p.id === personID);

			if (person === undefined) {
				throw new Error("People not found");
			}

			task.asignee = person;

			return "Success";
		},
		completeTask: (_: never, { taskID }: CompleteTaskInput): string => {
			const task = findTaskByID(taskID);

			if (task.done) {
				throw new Error("Task already done");
			}

			task.done = true;

			return "Success";
		},
		updateTask: (_: never, { taskID, modification }: UpdateTaskInput): Task => {
			const task = findTaskByID(taskID);

			task.title = modification.title || task.title;
			task.content = modification.content || task.content;

			return task;
		},
	},
	Person: {
		tasks: (parent: Person) => {
			const result = tasks.filter((t) => t.asignee?.id === parent.id);

			return result;
		},
	},
};

export default resolvers;
